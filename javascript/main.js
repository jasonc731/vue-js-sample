/**
 * Created by Jason on 4/9/2017.
 */

var app = new Vue({
    el: '#app',
    data: {
        link: 'http://google.com',
        attachRed: false
    },
    methods: {
        changeLink: function () {
            this.link = 'http://apple.com';
        }
    },
    watch: {
        counter: function(value) {
            var vm = this;
            setTimeout(function () {
               vm.counter = 0;
            }, 2000)
        }
    }
});